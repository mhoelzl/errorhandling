#include "try-catch.h"

#include <gmock/gmock.h>

using namespace ::testing;

// NOLINTNEXTLINE
TEST(TryCatch, ComputeSum_ComputesSumOfArguments)
{
	auto Result{ComputeSum(2.0, 5.0)};
	EXPECT_THAT(Result, DoubleEq(7.0));
}

// NOLINTNEXTLINE
TEST(TryCatch, ComputeSqrt_ComputesResult_WhenCalledWithZeroArg)
{
	auto Result{ComputeSqrt(0.0)};
	EXPECT_THAT(Result, DoubleEq(0.0));
}

// NOLINTNEXTLINE
TEST(TryCatch, ComputeSqrt_ComputesResult_WhenCalledWithPositiveArg)
{
	auto Result{ComputeSqrt(4.0)};
	EXPECT_THAT(Result, DoubleEq(2.0));
}

// NOLINTNEXTLINE
TEST(TryCatch, ComputeSqrt_Throws_WhenCalledWithNegativeArg)
{
	EXPECT_THROW(ComputeSqrt(-4.0), std::out_of_range); // NOLINT
}

// NOLINTNEXTLINE
TEST(TryCatch, AllocateDoubleArray_ReturnsArray_WhenArgsAreValid)
{
	auto Result{AllocateDoubleArray(10)};
	EXPECT_THAT(Result, Not(Eq(nullptr)));
}

// NOLINTNEXTLINE
TEST(TryCatch, AllocateDoubleArray_Throws_WhenArgsAreInvalid)
{
	EXPECT_THROW(AllocateDoubleArray(200), std::bad_alloc); // NOLINT
}

// NOLINTNEXTLINE
TEST(TryCatch, CreateArray_CreatesArray)
{
	constexpr int Size{20};
	auto Result{CreateArray(Size)};
	int Delta{Size / 2}; // NOLINT

	for (int i{0}; i < Delta; ++i)
	{
		EXPECT_THAT(Result[i], DoubleEq(Delta - i));
	}

	for (int i{Delta}; i < Size; ++i)
	{
		EXPECT_THAT(Result[i], DoubleEq(i - Delta));
	}
}
