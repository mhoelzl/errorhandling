#include "try-catch.h"

#include <cmath>
#include <memory>
#include <string>

using namespace std::string_literals;

double ComputeSum(double Lhs, double Rhs) noexcept
{
	return Lhs + Rhs;
}

double ComputeSqrt(double Arg)
{
	if (Arg < 0)
	{
		throw std::out_of_range{"Cannot compute square root of negative number."s};
	}
	return sqrt(Arg);
}

std::unique_ptr<double[]> AllocateDoubleArray(int Size)
{
	if (Size > 100)
	{
		throw std::bad_alloc{};
	}

	return std::make_unique<double[]>(Size);
}

std::unique_ptr<double[]> CreateArray(int Size)
{
	std::unique_ptr<double[]> Result{AllocateDoubleArray(Size)};
	int Delta{Size / 2}; // NOLINT
	for (int i = 0; i < Size; ++i)
	{
		// Using try/catch inside the normal flow of control.
		// This is definitely bad style!
		try
		{
			Result[i] = round(ComputeSqrt(i - Delta) * ComputeSqrt(i - Delta));
		}
		catch (std::out_of_range& x)
		{
			Result[i] = round(ComputeSqrt(Delta - i) * ComputeSqrt(Delta - i));
		}
	}
	return Result;
}
