#include "throw-only.h"

#include <cmath>
#include <memory>
#include <string>

using namespace std::string_literals;

double ComputeSum(double Lhs, double Rhs) noexcept
{
	return Lhs + Rhs;
}

double ComputeSqrt(double Arg)
{
	if (Arg < 0)
	{
		throw std::out_of_range{"Cannot compute square root of negative number."s};
	}
	return sqrt(Arg);
}

std::unique_ptr<double[]> AllocateDoubleArray(int Size)
{
	if (Size > 100)
	{
		throw std::bad_alloc{};
	}

	return std::make_unique<double[]>(Size);
}

std::unique_ptr<double[]> CreateArray(int Size)
{
	std::unique_ptr<double[]> Result{AllocateDoubleArray(Size)};
	int Delta{Size / 2}; // NOLINT
	for (int i = 0; i < Size; ++i)
	{
		const int Value{i - Delta >= 0 ? i - Delta : Delta - i};
		Result[i] = round(ComputeSqrt(Value) * ComputeSqrt(Value));
	}
	return Result;
}
