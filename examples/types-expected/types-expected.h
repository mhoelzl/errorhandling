#pragma once

#include "attributes.h"
#include "expected.hpp"
#include <memory>
#include <string>

NODISCARD tl::expected<double, std::string> ComputeSum(double Lhs, double Rhs) noexcept;
NODISCARD tl::expected<double, std::string> ComputeSqrt(double Arg) noexcept;
NODISCARD tl::expected<std::unique_ptr<double[]>, std::string> AllocateDoubleArray(int Size) noexcept;
NODISCARD tl::expected<std::unique_ptr<double[]>, std::string> CreateArray(int Size);