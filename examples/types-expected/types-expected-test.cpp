#include "types-expected.h"

#include <gmock/gmock.h>
#include <string>

using namespace ::testing;
using namespace std::string_literals;

// NOLINTNEXTLINE
TEST(TypesExpected, ComputeSum_ComputesSumOfArgumentsAndReturnsValue)
{
	auto Result{ComputeSum(2.0, 5.0)};
	EXPECT_TRUE(static_cast<bool>(Result));
	EXPECT_THAT(*Result, DoubleEq(7.0));
}

// NOLINTNEXTLINE
TEST(TypesExpected, ComputeSqrt_ComputesResultAndReturnsValue_WhenCalledWithZeroArg)
{
	auto Result{ComputeSqrt(0.0)};
	EXPECT_TRUE(static_cast<bool>(Result));
	EXPECT_THAT(*Result, DoubleEq(0.0));
}

// NOLINTNEXTLINE
TEST(TypesExpected, ComputeSqrt_ComputesResultAndReturnsValue_WhenCalledWithPositiveArg)
{
	auto Result{ComputeSqrt(4.0)};
	EXPECT_TRUE(static_cast<bool>(Result));
	EXPECT_THAT(*Result, DoubleEq(2.0));
}

// NOLINTNEXTLINE
TEST(TypesExpected, ComputeSqrt_ReturnsErrorCode_WhenCalledWithNegativeArg)
{
	auto Result{ComputeSqrt(-4.0)};
	EXPECT_FALSE(static_cast<bool>(Result));
	EXPECT_THAT(Result.error(), "Cannot compute square root of negative number."s);
}

// NOLINTNEXTLINE
TEST(TypesExpected, AllocateDoubleArray_ReturnsArray_WhenArgsAreValid)
{
	auto Result{AllocateDoubleArray(10)};
	EXPECT_TRUE(static_cast<bool>(Result));
	EXPECT_THAT(*Result, Not(Eq(nullptr)));
}

// NOLINTNEXTLINE
TEST(TypesExpected, AllocateDoubleArray_ReturnsErrorCode_WhenArgsAreInvalid)
{
	auto Result{AllocateDoubleArray(200)};
	EXPECT_FALSE(static_cast<bool>(Result));
	EXPECT_THAT(Result.error(), Eq("Cannot allocate double[] with size > 100."s));
}

// NOLINTNEXTLINE
TEST(TypesExpected, CreateArray_CreatesArray)
{
	constexpr int Size{20};
	auto Result{CreateArray(Size)};
	int Delta{Size / 2}; // NOLINT

	ASSERT_TRUE(Result.has_value());

	for (int i{0}; i < Delta; ++i)
	{
		EXPECT_THAT(Result.value()[i], DoubleEq(Delta - i));
	}

	for (int i{Delta}; i < Size; ++i)
	{
		EXPECT_THAT(Result.value()[i], DoubleEq(i - Delta));
	}
}
