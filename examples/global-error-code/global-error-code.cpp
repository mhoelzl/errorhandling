#include "global-error-code.h"
#include <cerrno>
#include <cmath>

void DomainError()
{
	errno = EDOM;
}

void OutOfMemoryError()
{
	errno = ENOMEM;
}

bool ComputeSum(double Lhs, double Rhs, double& Result) noexcept
{
	Result = Lhs + Rhs;
	return true;
}

bool ComputeSqrt(double Arg, double& Result) noexcept
{
	if (Arg < 0)
	{
		DomainError();
		return false;
	}
	Result = sqrt(Arg);
	return true;
}

bool AllocateDoubleArray(int Size, std::unique_ptr<double[]>& Result) noexcept
{
	if (Size > 100)
	{
		OutOfMemoryError();
		return false;
	}

	Result = std::make_unique<double[]>(static_cast<size_t>(Size));
	return true;
}

bool CreateArray(int Size, std::unique_ptr<double[]>& Result) noexcept
{
	auto Success{AllocateDoubleArray(Size, Result)};

	if (Success)
	{
		int Delta{Size / 2}; // NOLINT
		for (int i = 0; i < Size; ++i)
		{
			double Value;
			auto LocalSuccess{ComputeSqrt(i - Delta, Value)};

			if (!LocalSuccess)
			{
				static_cast<void>(ComputeSqrt(Delta - i, Value));
			}

			Result[i] = round(Value * Value);
		}
	}
	return Success;
}
