#include "global-error-code.h"

#include <gmock/gmock.h>

using namespace ::testing;

// NOLINTNEXTLINE
TEST(GlobalErrorCode, DomainError_SetsErrorCode)
{
	DomainError();
	EXPECT_THAT(errno, Eq(EDOM));
}

// NOLINTNEXTLINE
TEST(GlobalErrorCode, OutOfMemoryError_SetsErrorCode)
{
	OutOfMemoryError();
	EXPECT_THAT(errno, Eq(ENOMEM));
}

// NOLINTNEXTLINE
TEST(GlobalErrorCode, ComputeSum_ComputesSumOfArguments)
{
	double Result{};
	EXPECT_TRUE(ComputeSum(2.0, 5.0, Result));
	EXPECT_THAT(Result, DoubleEq(7.0));
}

// NOLINTNEXTLINE
TEST(GlobalErrorCode, ComputeSqrt_ComputesResultAndDoesNotSetErrorCode_WhenCalledWithZeroArg)
{
	double Result{};
	EXPECT_TRUE(ComputeSqrt(0.0, Result));
	EXPECT_THAT(Result, DoubleEq(0.0));
}

// NOLINTNEXTLINE
TEST(GlobalErrorCode, ComputeSqrt_ComputesResultAndDoesNotSetErrorCode_WhenCalledWithPositiveArg)
{
	double Result{};
	EXPECT_TRUE(ComputeSqrt(4.0, Result));
	EXPECT_THAT(Result, DoubleEq(2.0));
}

// NOLINTNEXTLINE
TEST(GlobalErrorCode, ComputeSqrt_SetsErrorCode_WhenCalledWithNegativeArg)
{
	double Result{};
	EXPECT_FALSE(ComputeSqrt(-4.0, Result));
	EXPECT_THAT(errno, Eq(EDOM));
}

// NOLINTNEXTLINE
TEST(GlobalErrorCode, AllocateDoubleArray_ReturnsArray_WhenArgsAreValid)
{
	std::unique_ptr<double[]> Result{};
	ASSERT_THAT(Result.get(), Eq(nullptr));

	EXPECT_TRUE(AllocateDoubleArray(10, Result));
	EXPECT_THAT(Result.get(), Not(Eq(nullptr)));
}

// NOLINTNEXTLINE
TEST(GlobalErrorCode, AllocateDoubleArray_SetsErrorCode_WhenArgsAreInvalid)
{
	std::unique_ptr<double[]> Result{};

	EXPECT_FALSE(AllocateDoubleArray(200, Result));
	EXPECT_THAT(errno, Eq(ENOMEM));
}

// NOLINTNEXTLINE
TEST(GlobalErrorCode, CreateArray_CreatesArray)
{
	constexpr int Size{20};
	std::unique_ptr<double[]> Result{};

	bool Success{CreateArray(Size, Result)};
	int Delta{Size / 2}; // NOLINT
	ASSERT_TRUE(Success);

	for (int i{0}; i < Delta; ++i)
	{
		EXPECT_THAT(Result[i], DoubleEq(Delta - i));
	}

	for (int i{Delta}; i < Size; ++i)
	{
		EXPECT_THAT(Result[i], DoubleEq(i - Delta));
	}
}
