#include <cstdlib>
#include <iostream>
#include <stdexcept>

#include "log-scope.h"

using namespace ::std::literals::string_literals;

int sum(int n, int count = 0)
{
	LogScope logScope{"sum", n};

	if (n <= 0)
	{
		if (count % 2 == 0)
		{
			throw "Argument out of range";
		}
		return 0;
	}

	return sum(n - 1, count + 1) + n;
}

void print_result(int input, int result) noexcept
{
	std::cout << "The sum from 1 to " << input << " is " << result << std::endl;
}

void print_error_message() noexcept
{
	std::cout << "Encountered an error somewhere during execution." << std::endl;
}

int main(int argc, char** argv)
{
	LogScope logScope{"main"};
	if (argc > 1)
	{
		try
		{
			int input{std::atoi(argv[1])};
			int result{sum(input)};
			print_result(input, result);
		}
		catch (...)
		{
			print_error_message();
		}
	}
	return 0;
}
