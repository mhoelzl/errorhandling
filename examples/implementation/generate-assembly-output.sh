#!/bin/bash

export CXX=clang++-6.0
export CLANGXX=clang++-6.0
export OPT=opt-6.0

mkdir -p assembly/sum-expected/dot
mkdir -p assembly/sum-exception/dot
mkdir -p assembly/sum-exception-value/dot

${CXX} -O2 -fno-inline -g -S -o assembly/sum-expected/sum-expected.s sum-expected.cpp
${CLANGXX} -O2 -S -o assembly/sum-expected/sum-expected.ll -emit-llvm sum-expected.cpp
(cd assembly/sum-expected/ && ${OPT} -S -o sum-expected.opt.ll --dot-cfg sum-expected.ll)
mv assembly/sum-expected/cfg* assembly/sum-expected/dot/
(cd assembly/sum-expected/dot/ && dot -O -Tpdf *.dot && mv *.pdf ..)
(cd assembly/sum-expected/ && mv cfg.main.dot.pdf sum-expected-main.pdf)

${CXX} -O2 -fno-inline -g -S -o assembly/sum-exception/sum-exception.s sum-exception.cpp
${CLANGXX} -O2 -S -o assembly/sum-exception/sum-exception.ll -emit-llvm sum-exception.cpp
(cd assembly/sum-exception/ && ${OPT} -S -o sum-exception.opt.ll --dot-cfg sum-exception.ll)
mv assembly/sum-exception/cfg* assembly/sum-exception/dot/
(cd assembly/sum-exception/dot/ && dot -O -Tpdf *.dot && mv *.pdf ..)
(cd assembly/sum-exception/ && mv cfg.main.dot.pdf sum-exception-main.pdf)

${CXX} -O2 -fno-inline -g -S -o assembly/sum-exception-value/sum-exception-value.s sum-exception-value.cpp
${CLANGXX} -O2 -S -o assembly/sum-exception-value/sum-exception-value.ll -emit-llvm sum-exception-value.cpp
(cd assembly/sum-exception-value/ && ${OPT} -S -o sum-exception-value.opt.ll --dot-cfg sum-exception-value.ll)
mv assembly/sum-exception-value/cfg* assembly/sum-exception-value/dot/
(cd assembly/sum-exception-value/dot/ && dot -O -Tpdf *.dot && mv *.pdf ..)
(cd assembly/sum-exception-value/ && mv cfg.main.dot.pdf sum-exception-value-main.pdf)

