; ModuleID = 'sum-exception-value.cpp'
source_filename = "sum-exception-value.cpp"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%"class.std::ios_base::Init" = type { i8 }
%"class.std::basic_ostream" = type { i32 (...)**, %"class.std::basic_ios" }
%"class.std::basic_ios" = type { %"class.std::ios_base", %"class.std::basic_ostream"*, i8, i8, %"class.std::basic_streambuf"*, %"class.std::ctype"*, %"class.std::num_put"*, %"class.std::num_get"* }
%"class.std::ios_base" = type { i32 (...)**, i64, i64, i32, i32, i32, %"struct.std::ios_base::_Callback_list"*, %"struct.std::ios_base::_Words", [8 x %"struct.std::ios_base::_Words"], i32, %"struct.std::ios_base::_Words"*, %"class.std::locale" }
%"struct.std::ios_base::_Callback_list" = type { %"struct.std::ios_base::_Callback_list"*, void (i32, %"class.std::ios_base"*, i32)*, i32, i32 }
%"struct.std::ios_base::_Words" = type { i8*, i64 }
%"class.std::locale" = type { %"class.std::locale::_Impl"* }
%"class.std::locale::_Impl" = type { i32, %"class.std::locale::facet"**, i64, %"class.std::locale::facet"**, i8** }
%"class.std::locale::facet" = type <{ i32 (...)**, i32, [4 x i8] }>
%"class.std::basic_streambuf" = type { i32 (...)**, i8*, i8*, i8*, i8*, i8*, i8*, %"class.std::locale" }
%"class.std::ctype" = type <{ %"class.std::locale::facet.base", [4 x i8], %struct.__locale_struct*, i8, [7 x i8], i32*, i32*, i16*, i8, [256 x i8], [256 x i8], i8, [6 x i8] }>
%"class.std::locale::facet.base" = type <{ i32 (...)**, i32 }>
%struct.__locale_struct = type { [13 x %struct.__locale_data*], i16*, i32*, i32*, [13 x i8*] }
%struct.__locale_data = type opaque
%"class.std::num_put" = type { %"class.std::locale::facet.base", [4 x i8] }
%"class.std::num_get" = type { %"class.std::locale::facet.base", [4 x i8] }
%class.LogScope = type { %"class.std::__cxx11::basic_string" }
%"class.std::__cxx11::basic_string" = type { %"struct.std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Alloc_hider", i64, %union.anon }
%"struct.std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Alloc_hider" = type { i8* }
%union.anon = type { i64, [8 x i8] }
%struct.__va_list_tag = type { i32, i32, i8*, i8* }

$_ZN8LogScopeC2IiEEPKcT_ = comdat any

$_ZN8LogScopeD2Ev = comdat any

$__clang_call_terminate = comdat any

$_ZN8LogScopeC2EPKc = comdat any

$_ZN8LogScopeC2ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE = comdat any

$_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z = comdat any

@_ZStL8__ioinit = internal global %"class.std::ios_base::Init" zeroinitializer, align 1
@__dso_handle = external hidden global i8
@.str = private unnamed_addr constant [4 x i8] c"sum\00", align 1
@.str.1 = private unnamed_addr constant [22 x i8] c"Argument out of range\00", align 1
@_ZTIPKc = external constant i8*
@_ZSt4cout = external global %"class.std::basic_ostream", align 8
@.str.2 = private unnamed_addr constant [19 x i8] c"The sum from 1 to \00", align 1
@.str.3 = private unnamed_addr constant [5 x i8] c" is \00", align 1
@.str.4 = private unnamed_addr constant [49 x i8] c"Encountered an error somewhere during execution.\00", align 1
@.str.5 = private unnamed_addr constant [5 x i8] c"main\00", align 1
@.str.6 = private unnamed_addr constant [16 x i8] c"Exiting scope: \00", align 1
@.str.7 = private unnamed_addr constant [42 x i8] c"basic_string::_M_construct null not valid\00", align 1
@.str.8 = private unnamed_addr constant [17 x i8] c"Entering scope: \00", align 1
@.str.9 = private unnamed_addr constant [2 x i8] c"(\00", align 1
@.str.10 = private unnamed_addr constant [2 x i8] c")\00", align 1
@.str.11 = private unnamed_addr constant [21 x i8] c"basic_string::append\00", align 1
@.str.14 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_sum_exception_value.cpp, i8* null }]

declare void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"*) unnamed_addr #0

; Function Attrs: nounwind
declare void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"*) unnamed_addr #1

; Function Attrs: nounwind
declare i32 @__cxa_atexit(void (i8*)*, i8*, i8*) local_unnamed_addr #2

; Function Attrs: uwtable
define i32 @_Z3sumii(i32, i32) local_unnamed_addr #3 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %3 = alloca %class.LogScope, align 8
  %4 = bitcast %class.LogScope* %3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* nonnull %4) #2
  call void @_ZN8LogScopeC2IiEEPKcT_(%class.LogScope* nonnull %3, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i64 0, i64 0), i32 %0) #2
  %5 = icmp slt i32 %0, 1
  br i1 %5, label %6, label %14

; <label>:6:                                      ; preds = %2
  %7 = and i32 %1, 1
  %8 = icmp eq i32 %7, 0
  br i1 %8, label %9, label %20

; <label>:9:                                      ; preds = %6
  %10 = call i8* @__cxa_allocate_exception(i64 8) #2
  %11 = bitcast i8* %10 to i8**
  store i8* getelementptr inbounds ([22 x i8], [22 x i8]* @.str.1, i64 0, i64 0), i8** %11, align 16, !tbaa !2
  invoke void @__cxa_throw(i8* %10, i8* bitcast (i8** @_ZTIPKc to i8*), i8* null) #11
          to label %22 unwind label %12

; <label>:12:                                     ; preds = %14, %9
  %13 = landingpad { i8*, i32 }
          cleanup
  call void @_ZN8LogScopeD2Ev(%class.LogScope* nonnull %3) #2
  call void @llvm.lifetime.end.p0i8(i64 32, i8* nonnull %4) #2
  resume { i8*, i32 } %13

; <label>:14:                                     ; preds = %2
  %15 = add nsw i32 %0, -1
  %16 = add nsw i32 %1, 1
  %17 = invoke i32 @_Z3sumii(i32 %15, i32 %16)
          to label %18 unwind label %12

; <label>:18:                                     ; preds = %14
  %19 = add nsw i32 %17, %0
  br label %20

; <label>:20:                                     ; preds = %6, %18
  %21 = phi i32 [ %19, %18 ], [ 0, %6 ]
  call void @_ZN8LogScopeD2Ev(%class.LogScope* nonnull %3) #2
  call void @llvm.lifetime.end.p0i8(i64 32, i8* nonnull %4) #2
  ret i32 %21

; <label>:22:                                     ; preds = %9
  unreachable
}

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.start.p0i8(i64, i8* nocapture) #4

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN8LogScopeC2IiEEPKcT_(%class.LogScope*, i8*, i32) unnamed_addr #5 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %4 = alloca i64, align 8
  %5 = alloca %"class.std::__cxx11::basic_string", align 8
  %6 = alloca %"class.std::__cxx11::basic_string", align 8
  %7 = alloca %"class.std::__cxx11::basic_string", align 8
  %8 = alloca %"class.std::__cxx11::basic_string", align 8
  %9 = alloca %"class.std::__cxx11::basic_string", align 8
  %10 = bitcast %"class.std::__cxx11::basic_string"* %6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* nonnull %10) #2
  %11 = bitcast %"class.std::__cxx11::basic_string"* %7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* nonnull %11) #2
  %12 = bitcast %"class.std::__cxx11::basic_string"* %8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* nonnull %12) #2
  %13 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %8, i64 0, i32 2
  %14 = bitcast %"class.std::__cxx11::basic_string"* %8 to %union.anon**
  store %union.anon* %13, %union.anon** %14, align 8, !tbaa !6
  %15 = icmp eq i8* %1, null
  %16 = bitcast %union.anon* %13 to i8*
  br i1 %15, label %21, label %17

; <label>:17:                                     ; preds = %3
  %18 = call i64 @strlen(i8* nonnull %1) #2
  %19 = bitcast i64* %4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* nonnull %19) #2
  store i64 %18, i64* %4, align 8, !tbaa !8
  %20 = icmp ugt i64 %18, 15
  br i1 %20, label %25, label %23

; <label>:21:                                     ; preds = %3
  invoke void @_ZSt19__throw_logic_errorPKc(i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.7, i64 0, i64 0)) #11
          to label %22 unwind label %172

; <label>:22:                                     ; preds = %21
  unreachable

; <label>:23:                                     ; preds = %17
  %24 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %8, i64 0, i32 0, i32 0
  br label %31

; <label>:25:                                     ; preds = %17
  %26 = invoke i8* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm(%"class.std::__cxx11::basic_string"* nonnull %8, i64* nonnull dereferenceable(8) %4, i64 0)
          to label %27 unwind label %172

; <label>:27:                                     ; preds = %25
  %28 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %8, i64 0, i32 0, i32 0
  store i8* %26, i8** %28, align 8, !tbaa !10
  %29 = load i64, i64* %4, align 8, !tbaa !8
  %30 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %8, i64 0, i32 2, i32 0
  store i64 %29, i64* %30, align 8, !tbaa !12
  br label %31

; <label>:31:                                     ; preds = %27, %23
  %32 = phi i8** [ %24, %23 ], [ %28, %27 ]
  %33 = phi i8* [ %16, %23 ], [ %26, %27 ]
  switch i64 %18, label %36 [
    i64 1, label %34
    i64 0, label %37
  ]

; <label>:34:                                     ; preds = %31
  %35 = load i8, i8* %1, align 1, !tbaa !12
  store i8 %35, i8* %33, align 1, !tbaa !12
  br label %37

; <label>:36:                                     ; preds = %31
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %33, i8* nonnull %1, i64 %18, i32 1, i1 false) #2
  br label %37

; <label>:37:                                     ; preds = %36, %34, %31
  %38 = load i64, i64* %4, align 8, !tbaa !8
  %39 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %8, i64 0, i32 1
  store i64 %38, i64* %39, align 8, !tbaa !13
  %40 = load i8*, i8** %32, align 8, !tbaa !10
  %41 = getelementptr inbounds i8, i8* %40, i64 %38
  store i8 0, i8* %41, align 1, !tbaa !12
  call void @llvm.lifetime.end.p0i8(i64 8, i8* nonnull %19) #2
  %42 = load i64, i64* %39, align 8, !tbaa !13, !noalias !14
  %43 = icmp eq i64 %42, 9223372036854775807
  br i1 %43, label %44, label %46

; <label>:44:                                     ; preds = %37
  invoke void @_ZSt20__throw_length_errorPKc(i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.11, i64 0, i64 0)) #11
          to label %45 unwind label %175

; <label>:45:                                     ; preds = %44
  unreachable

; <label>:46:                                     ; preds = %37
  %47 = invoke dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm(%"class.std::__cxx11::basic_string"* nonnull %8, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.9, i64 0, i64 0), i64 1)
          to label %48 unwind label %175

; <label>:48:                                     ; preds = %46
  %49 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %7, i64 0, i32 2
  %50 = bitcast %"class.std::__cxx11::basic_string"* %7 to %union.anon**
  store %union.anon* %49, %union.anon** %50, align 8, !tbaa !6, !alias.scope !14
  %51 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %47, i64 0, i32 0, i32 0
  %52 = load i8*, i8** %51, align 8, !tbaa !10
  %53 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %47, i64 0, i32 2
  %54 = bitcast %union.anon* %53 to i8*
  %55 = icmp eq i8* %52, %54
  br i1 %55, label %56, label %58

; <label>:56:                                     ; preds = %48
  %57 = bitcast %union.anon* %49 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* nonnull %57, i8* nonnull %52, i64 16, i32 1, i1 false) #2
  br label %63

; <label>:58:                                     ; preds = %48
  %59 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %7, i64 0, i32 0, i32 0
  store i8* %52, i8** %59, align 8, !tbaa !10, !alias.scope !14
  %60 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %47, i64 0, i32 2, i32 0
  %61 = load i64, i64* %60, align 8, !tbaa !12
  %62 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %7, i64 0, i32 2, i32 0
  store i64 %61, i64* %62, align 8, !tbaa !12, !alias.scope !14
  br label %63

; <label>:63:                                     ; preds = %58, %56
  %64 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %47, i64 0, i32 1
  %65 = load i64, i64* %64, align 8, !tbaa !13
  %66 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %7, i64 0, i32 1
  store i64 %65, i64* %66, align 8, !tbaa !13, !alias.scope !14
  %67 = bitcast %"class.std::__cxx11::basic_string"* %47 to %union.anon**
  store %union.anon* %53, %union.anon** %67, align 8, !tbaa !10
  store i64 0, i64* %64, align 8, !tbaa !13
  store i8 0, i8* %54, align 1, !tbaa !12
  %68 = bitcast %"class.std::__cxx11::basic_string"* %9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* nonnull %68) #2
  invoke void (%"class.std::__cxx11::basic_string"*, i32 (i8*, i64, i8*, %struct.__va_list_tag*)*, i64, i8*, ...) @_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z(%"class.std::__cxx11::basic_string"* nonnull sret %9, i32 (i8*, i64, i8*, %struct.__va_list_tag*)* nonnull @vsnprintf, i64 16, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.14, i64 0, i64 0), i32 %2)
          to label %69 unwind label %178

; <label>:69:                                     ; preds = %63
  %70 = load i64, i64* %66, align 8, !tbaa !13, !noalias !17
  %71 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %9, i64 0, i32 1
  %72 = load i64, i64* %71, align 8, !tbaa !13, !noalias !17
  %73 = add i64 %72, %70
  %74 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %7, i64 0, i32 0, i32 0
  %75 = load i8*, i8** %74, align 8, !tbaa !10, !noalias !17
  %76 = bitcast %union.anon* %49 to i8*
  %77 = icmp eq i8* %75, %76
  %78 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %7, i64 0, i32 2, i32 0
  %79 = load i64, i64* %78, align 8, !noalias !17
  %80 = select i1 %77, i64 15, i64 %79
  %81 = icmp ugt i64 %73, %80
  %82 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %9, i64 0, i32 0, i32 0
  %83 = load i8*, i8** %82, align 8, !tbaa !10, !noalias !17
  br i1 %81, label %84, label %94

; <label>:84:                                     ; preds = %69
  %85 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %9, i64 0, i32 2
  %86 = bitcast %union.anon* %85 to i8*
  %87 = icmp eq i8* %83, %86
  %88 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %9, i64 0, i32 2, i32 0
  %89 = load i64, i64* %88, align 8, !noalias !17
  %90 = select i1 %87, i64 15, i64 %89
  %91 = icmp ugt i64 %73, %90
  br i1 %91, label %94, label %92

; <label>:92:                                     ; preds = %84
  %93 = invoke dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm(%"class.std::__cxx11::basic_string"* nonnull %9, i64 0, i64 0, i8* %75, i64 %70)
          to label %96 unwind label %183

; <label>:94:                                     ; preds = %84, %69
  %95 = invoke dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm(%"class.std::__cxx11::basic_string"* nonnull %7, i8* %83, i64 %72)
          to label %96 unwind label %183

; <label>:96:                                     ; preds = %94, %92
  %97 = phi %"class.std::__cxx11::basic_string"* [ %93, %92 ], [ %95, %94 ]
  %98 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %6, i64 0, i32 2
  %99 = bitcast %"class.std::__cxx11::basic_string"* %6 to %union.anon**
  store %union.anon* %98, %union.anon** %99, align 8, !tbaa !6, !alias.scope !17
  %100 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %97, i64 0, i32 0, i32 0
  %101 = load i8*, i8** %100, align 8, !tbaa !10
  %102 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %97, i64 0, i32 2
  %103 = bitcast %union.anon* %102 to i8*
  %104 = icmp eq i8* %101, %103
  br i1 %104, label %105, label %107

; <label>:105:                                    ; preds = %96
  %106 = bitcast %union.anon* %98 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* nonnull %106, i8* nonnull %101, i64 16, i32 1, i1 false) #2
  br label %112

; <label>:107:                                    ; preds = %96
  %108 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %6, i64 0, i32 0, i32 0
  store i8* %101, i8** %108, align 8, !tbaa !10, !alias.scope !17
  %109 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %97, i64 0, i32 2, i32 0
  %110 = load i64, i64* %109, align 8, !tbaa !12
  %111 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %6, i64 0, i32 2, i32 0
  store i64 %110, i64* %111, align 8, !tbaa !12, !alias.scope !17
  br label %112

; <label>:112:                                    ; preds = %107, %105
  %113 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %97, i64 0, i32 1
  %114 = load i64, i64* %113, align 8, !tbaa !13
  %115 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %6, i64 0, i32 1
  store i64 %114, i64* %115, align 8, !tbaa !13, !alias.scope !17
  %116 = bitcast %"class.std::__cxx11::basic_string"* %97 to %union.anon**
  store %union.anon* %102, %union.anon** %116, align 8, !tbaa !10
  store i64 0, i64* %113, align 8, !tbaa !13
  store i8 0, i8* %103, align 1, !tbaa !12
  %117 = load i64, i64* %115, align 8, !tbaa !13, !noalias !20
  %118 = icmp eq i64 %117, 9223372036854775807
  br i1 %118, label %119, label %121

; <label>:119:                                    ; preds = %112
  invoke void @_ZSt20__throw_length_errorPKc(i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.11, i64 0, i64 0)) #11
          to label %120 unwind label %186

; <label>:120:                                    ; preds = %119
  unreachable

; <label>:121:                                    ; preds = %112
  %122 = invoke dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm(%"class.std::__cxx11::basic_string"* nonnull %6, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.10, i64 0, i64 0), i64 1)
          to label %123 unwind label %186

; <label>:123:                                    ; preds = %121
  %124 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %5, i64 0, i32 2
  %125 = bitcast %"class.std::__cxx11::basic_string"* %5 to %union.anon**
  store %union.anon* %124, %union.anon** %125, align 8, !tbaa !6, !alias.scope !20
  %126 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %122, i64 0, i32 0, i32 0
  %127 = load i8*, i8** %126, align 8, !tbaa !10
  %128 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %122, i64 0, i32 2
  %129 = bitcast %union.anon* %128 to i8*
  %130 = icmp eq i8* %127, %129
  br i1 %130, label %131, label %134

; <label>:131:                                    ; preds = %123
  %132 = bitcast %union.anon* %124 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* nonnull %132, i8* nonnull %127, i64 16, i32 1, i1 false) #2
  %133 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %5, i64 0, i32 0, i32 0
  br label %140

; <label>:134:                                    ; preds = %123
  %135 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %5, i64 0, i32 0, i32 0
  store i8* %127, i8** %135, align 8, !tbaa !10, !alias.scope !20
  %136 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %122, i64 0, i32 2, i32 0
  %137 = load i64, i64* %136, align 8, !tbaa !12
  %138 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %5, i64 0, i32 2, i32 0
  store i64 %137, i64* %138, align 8, !tbaa !12, !alias.scope !20
  %139 = bitcast %union.anon* %124 to i8*
  br label %140

; <label>:140:                                    ; preds = %134, %131
  %141 = phi i8* [ %139, %134 ], [ %132, %131 ]
  %142 = phi i8** [ %135, %134 ], [ %133, %131 ]
  %143 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %122, i64 0, i32 1
  %144 = load i64, i64* %143, align 8, !tbaa !13
  %145 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %5, i64 0, i32 1
  store i64 %144, i64* %145, align 8, !tbaa !13, !alias.scope !20
  %146 = bitcast %"class.std::__cxx11::basic_string"* %122 to %union.anon**
  store %union.anon* %128, %union.anon** %146, align 8, !tbaa !10
  store i64 0, i64* %143, align 8, !tbaa !13
  store i8 0, i8* %129, align 1, !tbaa !12
  call void @_ZN8LogScopeC2ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.LogScope* %0, %"class.std::__cxx11::basic_string"* nonnull %5) #2
  %147 = load i8*, i8** %142, align 8, !tbaa !10
  %148 = icmp eq i8* %147, %141
  br i1 %148, label %150, label %149

; <label>:149:                                    ; preds = %140
  call void @_ZdlPv(i8* %147) #2
  br label %150

; <label>:150:                                    ; preds = %140, %149
  %151 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %6, i64 0, i32 0, i32 0
  %152 = load i8*, i8** %151, align 8, !tbaa !10
  %153 = bitcast %union.anon* %98 to i8*
  %154 = icmp eq i8* %152, %153
  br i1 %154, label %156, label %155

; <label>:155:                                    ; preds = %150
  call void @_ZdlPv(i8* %152) #2
  br label %156

; <label>:156:                                    ; preds = %150, %155
  %157 = load i8*, i8** %82, align 8, !tbaa !10
  %158 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %9, i64 0, i32 2
  %159 = bitcast %union.anon* %158 to i8*
  %160 = icmp eq i8* %157, %159
  br i1 %160, label %162, label %161

; <label>:161:                                    ; preds = %156
  call void @_ZdlPv(i8* %157) #2
  br label %162

; <label>:162:                                    ; preds = %156, %161
  call void @llvm.lifetime.end.p0i8(i64 32, i8* nonnull %68) #2
  %163 = load i8*, i8** %74, align 8, !tbaa !10
  %164 = icmp eq i8* %163, %76
  br i1 %164, label %166, label %165

; <label>:165:                                    ; preds = %162
  call void @_ZdlPv(i8* %163) #2
  br label %166

; <label>:166:                                    ; preds = %162, %165
  %167 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %8, i64 0, i32 0, i32 0
  %168 = load i8*, i8** %167, align 8, !tbaa !10
  %169 = icmp eq i8* %168, %16
  br i1 %169, label %171, label %170

; <label>:170:                                    ; preds = %166
  call void @_ZdlPv(i8* %168) #2
  br label %171

; <label>:171:                                    ; preds = %166, %170
  call void @llvm.lifetime.end.p0i8(i64 32, i8* nonnull %12) #2
  call void @llvm.lifetime.end.p0i8(i64 32, i8* nonnull %11) #2
  call void @llvm.lifetime.end.p0i8(i64 32, i8* nonnull %10) #2
  ret void

; <label>:172:                                    ; preds = %25, %21
  %173 = landingpad { i8*, i32 }
          catch i8* null
  %174 = extractvalue { i8*, i32 } %173, 0
  br label %214

; <label>:175:                                    ; preds = %46, %44
  %176 = landingpad { i8*, i32 }
          catch i8* null
  %177 = extractvalue { i8*, i32 } %176, 0
  br label %208

; <label>:178:                                    ; preds = %63
  %179 = landingpad { i8*, i32 }
          catch i8* null
  %180 = extractvalue { i8*, i32 } %179, 0
  %181 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %7, i64 0, i32 0, i32 0
  %182 = bitcast %union.anon* %49 to i8*
  br label %201

; <label>:183:                                    ; preds = %94, %92
  %184 = landingpad { i8*, i32 }
          catch i8* null
  %185 = extractvalue { i8*, i32 } %184, 0
  br label %194

; <label>:186:                                    ; preds = %121, %119
  %187 = landingpad { i8*, i32 }
          catch i8* null
  %188 = extractvalue { i8*, i32 } %187, 0
  %189 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %6, i64 0, i32 0, i32 0
  %190 = load i8*, i8** %189, align 8, !tbaa !10
  %191 = bitcast %union.anon* %98 to i8*
  %192 = icmp eq i8* %190, %191
  br i1 %192, label %194, label %193

; <label>:193:                                    ; preds = %186
  call void @_ZdlPv(i8* %190) #2
  br label %194

; <label>:194:                                    ; preds = %193, %186, %183
  %195 = phi i8* [ %185, %183 ], [ %188, %186 ], [ %188, %193 ]
  %196 = load i8*, i8** %82, align 8, !tbaa !10
  %197 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %9, i64 0, i32 2
  %198 = bitcast %union.anon* %197 to i8*
  %199 = icmp eq i8* %196, %198
  br i1 %199, label %201, label %200

; <label>:200:                                    ; preds = %194
  call void @_ZdlPv(i8* %196) #2
  br label %201

; <label>:201:                                    ; preds = %200, %194, %178
  %202 = phi i8* [ %76, %200 ], [ %76, %194 ], [ %182, %178 ]
  %203 = phi i8** [ %74, %200 ], [ %74, %194 ], [ %181, %178 ]
  %204 = phi i8* [ %195, %200 ], [ %195, %194 ], [ %180, %178 ]
  call void @llvm.lifetime.end.p0i8(i64 32, i8* nonnull %68) #2
  %205 = load i8*, i8** %203, align 8, !tbaa !10
  %206 = icmp eq i8* %205, %202
  br i1 %206, label %208, label %207

; <label>:207:                                    ; preds = %201
  call void @_ZdlPv(i8* %205) #2
  br label %208

; <label>:208:                                    ; preds = %207, %201, %175
  %209 = phi i8* [ %177, %175 ], [ %204, %201 ], [ %204, %207 ]
  %210 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %8, i64 0, i32 0, i32 0
  %211 = load i8*, i8** %210, align 8, !tbaa !10
  %212 = icmp eq i8* %211, %16
  br i1 %212, label %214, label %213

; <label>:213:                                    ; preds = %208
  call void @_ZdlPv(i8* %211) #2
  br label %214

; <label>:214:                                    ; preds = %213, %208, %172
  %215 = phi i8* [ %174, %172 ], [ %209, %208 ], [ %209, %213 ]
  call void @llvm.lifetime.end.p0i8(i64 32, i8* nonnull %12) #2
  call void @llvm.lifetime.end.p0i8(i64 32, i8* nonnull %11) #2
  call void @llvm.lifetime.end.p0i8(i64 32, i8* nonnull %10) #2
  call void @__clang_call_terminate(i8* %215) #12
  unreachable
}

declare i8* @__cxa_allocate_exception(i64) local_unnamed_addr

declare void @__cxa_throw(i8*, i8*, i8*) local_unnamed_addr

declare i32 @__gxx_personality_v0(...)

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN8LogScopeD2Ev(%class.LogScope*) unnamed_addr #5 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %2 = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l(%"class.std::basic_ostream"* nonnull dereferenceable(272) @_ZSt4cout, i8* nonnull getelementptr inbounds ([16 x i8], [16 x i8]* @.str.6, i64 0, i64 0), i64 15)
          to label %3 unwind label %49

; <label>:3:                                      ; preds = %1
  %4 = getelementptr inbounds %class.LogScope, %class.LogScope* %0, i64 0, i32 0, i32 0, i32 0
  %5 = load i8*, i8** %4, align 8, !tbaa !10
  %6 = getelementptr inbounds %class.LogScope, %class.LogScope* %0, i64 0, i32 0, i32 1
  %7 = load i64, i64* %6, align 8, !tbaa !13
  %8 = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l(%"class.std::basic_ostream"* nonnull dereferenceable(272) @_ZSt4cout, i8* %5, i64 %7)
          to label %9 unwind label %49

; <label>:9:                                      ; preds = %3
  %10 = bitcast %"class.std::basic_ostream"* %8 to i8**
  %11 = load i8*, i8** %10, align 8, !tbaa !23
  %12 = getelementptr i8, i8* %11, i64 -24
  %13 = bitcast i8* %12 to i64*
  %14 = load i64, i64* %13, align 8
  %15 = bitcast %"class.std::basic_ostream"* %8 to i8*
  %16 = getelementptr inbounds i8, i8* %15, i64 %14
  %17 = getelementptr inbounds i8, i8* %16, i64 240
  %18 = bitcast i8* %17 to %"class.std::ctype"**
  %19 = load %"class.std::ctype"*, %"class.std::ctype"** %18, align 8, !tbaa !25
  %20 = icmp eq %"class.std::ctype"* %19, null
  br i1 %20, label %21, label %23

; <label>:21:                                     ; preds = %9
  invoke void @_ZSt16__throw_bad_castv() #11
          to label %22 unwind label %49

; <label>:22:                                     ; preds = %21
  unreachable

; <label>:23:                                     ; preds = %9
  %24 = getelementptr inbounds %"class.std::ctype", %"class.std::ctype"* %19, i64 0, i32 8
  %25 = load i8, i8* %24, align 8, !tbaa !28
  %26 = icmp eq i8 %25, 0
  br i1 %26, label %30, label %27

; <label>:27:                                     ; preds = %23
  %28 = getelementptr inbounds %"class.std::ctype", %"class.std::ctype"* %19, i64 0, i32 9, i64 10
  %29 = load i8, i8* %28, align 1, !tbaa !12
  br label %37

; <label>:30:                                     ; preds = %23
  invoke void @_ZNKSt5ctypeIcE13_M_widen_initEv(%"class.std::ctype"* nonnull %19)
          to label %31 unwind label %49

; <label>:31:                                     ; preds = %30
  %32 = bitcast %"class.std::ctype"* %19 to i8 (%"class.std::ctype"*, i8)***
  %33 = load i8 (%"class.std::ctype"*, i8)**, i8 (%"class.std::ctype"*, i8)*** %32, align 8, !tbaa !23
  %34 = getelementptr inbounds i8 (%"class.std::ctype"*, i8)*, i8 (%"class.std::ctype"*, i8)** %33, i64 6
  %35 = load i8 (%"class.std::ctype"*, i8)*, i8 (%"class.std::ctype"*, i8)** %34, align 8
  %36 = invoke signext i8 %35(%"class.std::ctype"* nonnull %19, i8 signext 10)
          to label %37 unwind label %49

; <label>:37:                                     ; preds = %31, %27
  %38 = phi i8 [ %29, %27 ], [ %36, %31 ]
  %39 = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZNSo3putEc(%"class.std::basic_ostream"* nonnull %8, i8 signext %38)
          to label %40 unwind label %49

; <label>:40:                                     ; preds = %37
  %41 = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZNSo5flushEv(%"class.std::basic_ostream"* nonnull %39)
          to label %42 unwind label %49

; <label>:42:                                     ; preds = %40
  %43 = load i8*, i8** %4, align 8, !tbaa !10
  %44 = getelementptr inbounds %class.LogScope, %class.LogScope* %0, i64 0, i32 0, i32 2
  %45 = bitcast %union.anon* %44 to i8*
  %46 = icmp eq i8* %43, %45
  br i1 %46, label %48, label %47

; <label>:47:                                     ; preds = %42
  tail call void @_ZdlPv(i8* %43) #2
  br label %48

; <label>:48:                                     ; preds = %42, %47
  ret void

; <label>:49:                                     ; preds = %21, %31, %30, %40, %37, %3, %1
  %50 = landingpad { i8*, i32 }
          filter [0 x i8*] zeroinitializer
  %51 = extractvalue { i8*, i32 } %50, 0
  %52 = getelementptr inbounds %class.LogScope, %class.LogScope* %0, i64 0, i32 0
  tail call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev(%"class.std::__cxx11::basic_string"* %52) #2
  tail call void @__cxa_call_unexpected(i8* %51) #12
  unreachable
}

; Function Attrs: argmemonly nounwind
declare void @llvm.lifetime.end.p0i8(i64, i8* nocapture) #4

; Function Attrs: nounwind uwtable
define void @_Z12print_resultii(i32, i32) local_unnamed_addr #5 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %3 = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l(%"class.std::basic_ostream"* nonnull dereferenceable(272) @_ZSt4cout, i8* nonnull getelementptr inbounds ([19 x i8], [19 x i8]* @.str.2, i64 0, i64 0), i64 18)
          to label %4 unwind label %44

; <label>:4:                                      ; preds = %2
  %5 = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* nonnull @_ZSt4cout, i32 %0)
          to label %6 unwind label %44

; <label>:6:                                      ; preds = %4
  %7 = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l(%"class.std::basic_ostream"* nonnull dereferenceable(272) %5, i8* nonnull getelementptr inbounds ([5 x i8], [5 x i8]* @.str.3, i64 0, i64 0), i64 4)
          to label %8 unwind label %44

; <label>:8:                                      ; preds = %6
  %9 = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* nonnull %5, i32 %1)
          to label %10 unwind label %44

; <label>:10:                                     ; preds = %8
  %11 = bitcast %"class.std::basic_ostream"* %9 to i8**
  %12 = load i8*, i8** %11, align 8, !tbaa !23
  %13 = getelementptr i8, i8* %12, i64 -24
  %14 = bitcast i8* %13 to i64*
  %15 = load i64, i64* %14, align 8
  %16 = bitcast %"class.std::basic_ostream"* %9 to i8*
  %17 = getelementptr inbounds i8, i8* %16, i64 %15
  %18 = getelementptr inbounds i8, i8* %17, i64 240
  %19 = bitcast i8* %18 to %"class.std::ctype"**
  %20 = load %"class.std::ctype"*, %"class.std::ctype"** %19, align 8, !tbaa !25
  %21 = icmp eq %"class.std::ctype"* %20, null
  br i1 %21, label %22, label %24

; <label>:22:                                     ; preds = %10
  invoke void @_ZSt16__throw_bad_castv() #11
          to label %23 unwind label %44

; <label>:23:                                     ; preds = %22
  unreachable

; <label>:24:                                     ; preds = %10
  %25 = getelementptr inbounds %"class.std::ctype", %"class.std::ctype"* %20, i64 0, i32 8
  %26 = load i8, i8* %25, align 8, !tbaa !28
  %27 = icmp eq i8 %26, 0
  br i1 %27, label %31, label %28

; <label>:28:                                     ; preds = %24
  %29 = getelementptr inbounds %"class.std::ctype", %"class.std::ctype"* %20, i64 0, i32 9, i64 10
  %30 = load i8, i8* %29, align 1, !tbaa !12
  br label %38

; <label>:31:                                     ; preds = %24
  invoke void @_ZNKSt5ctypeIcE13_M_widen_initEv(%"class.std::ctype"* nonnull %20)
          to label %32 unwind label %44

; <label>:32:                                     ; preds = %31
  %33 = bitcast %"class.std::ctype"* %20 to i8 (%"class.std::ctype"*, i8)***
  %34 = load i8 (%"class.std::ctype"*, i8)**, i8 (%"class.std::ctype"*, i8)*** %33, align 8, !tbaa !23
  %35 = getelementptr inbounds i8 (%"class.std::ctype"*, i8)*, i8 (%"class.std::ctype"*, i8)** %34, i64 6
  %36 = load i8 (%"class.std::ctype"*, i8)*, i8 (%"class.std::ctype"*, i8)** %35, align 8
  %37 = invoke signext i8 %36(%"class.std::ctype"* nonnull %20, i8 signext 10)
          to label %38 unwind label %44

; <label>:38:                                     ; preds = %32, %28
  %39 = phi i8 [ %30, %28 ], [ %37, %32 ]
  %40 = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZNSo3putEc(%"class.std::basic_ostream"* nonnull %9, i8 signext %39)
          to label %41 unwind label %44

; <label>:41:                                     ; preds = %38
  %42 = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZNSo5flushEv(%"class.std::basic_ostream"* nonnull %40)
          to label %43 unwind label %44

; <label>:43:                                     ; preds = %41
  ret void

; <label>:44:                                     ; preds = %22, %32, %31, %41, %38, %6, %2, %8, %4
  %45 = landingpad { i8*, i32 }
          catch i8* null
  %46 = extractvalue { i8*, i32 } %45, 0
  tail call void @__clang_call_terminate(i8* %46) #12
  unreachable
}

; Function Attrs: noinline noreturn nounwind
define linkonce_odr hidden void @__clang_call_terminate(i8*) local_unnamed_addr #6 comdat {
  %2 = tail call i8* @__cxa_begin_catch(i8* %0) #2
  tail call void @_ZSt9terminatev() #12
  unreachable
}

declare i8* @__cxa_begin_catch(i8*) local_unnamed_addr

declare void @_ZSt9terminatev() local_unnamed_addr

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"*, i32) local_unnamed_addr #0

; Function Attrs: nounwind uwtable
define void @_Z19print_error_messagev() local_unnamed_addr #5 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %1 = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l(%"class.std::basic_ostream"* nonnull dereferenceable(272) @_ZSt4cout, i8* nonnull getelementptr inbounds ([49 x i8], [49 x i8]* @.str.4, i64 0, i64 0), i64 48)
          to label %2 unwind label %34

; <label>:2:                                      ; preds = %0
  %3 = load i8*, i8** bitcast (%"class.std::basic_ostream"* @_ZSt4cout to i8**), align 8, !tbaa !23
  %4 = getelementptr i8, i8* %3, i64 -24
  %5 = bitcast i8* %4 to i64*
  %6 = load i64, i64* %5, align 8
  %7 = getelementptr inbounds i8, i8* bitcast (%"class.std::basic_ostream"* @_ZSt4cout to i8*), i64 %6
  %8 = getelementptr inbounds i8, i8* %7, i64 240
  %9 = bitcast i8* %8 to %"class.std::ctype"**
  %10 = load %"class.std::ctype"*, %"class.std::ctype"** %9, align 8, !tbaa !25
  %11 = icmp eq %"class.std::ctype"* %10, null
  br i1 %11, label %12, label %14

; <label>:12:                                     ; preds = %2
  invoke void @_ZSt16__throw_bad_castv() #11
          to label %13 unwind label %34

; <label>:13:                                     ; preds = %12
  unreachable

; <label>:14:                                     ; preds = %2
  %15 = getelementptr inbounds %"class.std::ctype", %"class.std::ctype"* %10, i64 0, i32 8
  %16 = load i8, i8* %15, align 8, !tbaa !28
  %17 = icmp eq i8 %16, 0
  br i1 %17, label %21, label %18

; <label>:18:                                     ; preds = %14
  %19 = getelementptr inbounds %"class.std::ctype", %"class.std::ctype"* %10, i64 0, i32 9, i64 10
  %20 = load i8, i8* %19, align 1, !tbaa !12
  br label %28

; <label>:21:                                     ; preds = %14
  invoke void @_ZNKSt5ctypeIcE13_M_widen_initEv(%"class.std::ctype"* nonnull %10)
          to label %22 unwind label %34

; <label>:22:                                     ; preds = %21
  %23 = bitcast %"class.std::ctype"* %10 to i8 (%"class.std::ctype"*, i8)***
  %24 = load i8 (%"class.std::ctype"*, i8)**, i8 (%"class.std::ctype"*, i8)*** %23, align 8, !tbaa !23
  %25 = getelementptr inbounds i8 (%"class.std::ctype"*, i8)*, i8 (%"class.std::ctype"*, i8)** %24, i64 6
  %26 = load i8 (%"class.std::ctype"*, i8)*, i8 (%"class.std::ctype"*, i8)** %25, align 8
  %27 = invoke signext i8 %26(%"class.std::ctype"* nonnull %10, i8 signext 10)
          to label %28 unwind label %34

; <label>:28:                                     ; preds = %22, %18
  %29 = phi i8 [ %20, %18 ], [ %27, %22 ]
  %30 = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZNSo3putEc(%"class.std::basic_ostream"* nonnull @_ZSt4cout, i8 signext %29)
          to label %31 unwind label %34

; <label>:31:                                     ; preds = %28
  %32 = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZNSo5flushEv(%"class.std::basic_ostream"* nonnull %30)
          to label %33 unwind label %34

; <label>:33:                                     ; preds = %31
  ret void

; <label>:34:                                     ; preds = %31, %28, %22, %21, %12, %0
  %35 = landingpad { i8*, i32 }
          catch i8* null
  %36 = extractvalue { i8*, i32 } %35, 0
  tail call void @__clang_call_terminate(i8* %36) #12
  unreachable
}

; Function Attrs: norecurse uwtable
define i32 @main(i32, i8** nocapture readonly) local_unnamed_addr #7 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %3 = alloca %class.LogScope, align 8
  %4 = bitcast %class.LogScope* %3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* nonnull %4) #2
  call void @_ZN8LogScopeC2EPKc(%class.LogScope* nonnull %3, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.5, i64 0, i64 0)) #2
  %5 = icmp sgt i32 %0, 1
  br i1 %5, label %6, label %19

; <label>:6:                                      ; preds = %2
  %7 = getelementptr inbounds i8*, i8** %1, i64 1
  %8 = load i8*, i8** %7, align 8, !tbaa !2
  %9 = call i64 @strtol(i8* nocapture nonnull %8, i8** null, i32 10) #2
  %10 = trunc i64 %9 to i32
  %11 = invoke i32 @_Z3sumii(i32 %10, i32 0)
          to label %12 unwind label %13

; <label>:12:                                     ; preds = %6
  call void @_Z12print_resultii(i32 %10, i32 %11) #2
  br label %19

; <label>:13:                                     ; preds = %6
  %14 = landingpad { i8*, i32 }
          catch i8* null
  %15 = extractvalue { i8*, i32 } %14, 0
  %16 = call i8* @__cxa_begin_catch(i8* %15) #2
  call void @_Z19print_error_messagev() #2
  invoke void @__cxa_end_catch()
          to label %19 unwind label %17

; <label>:17:                                     ; preds = %13
  %18 = landingpad { i8*, i32 }
          cleanup
  call void @_ZN8LogScopeD2Ev(%class.LogScope* nonnull %3) #2
  call void @llvm.lifetime.end.p0i8(i64 32, i8* nonnull %4) #2
  resume { i8*, i32 } %18

; <label>:19:                                     ; preds = %12, %13, %2
  call void @_ZN8LogScopeD2Ev(%class.LogScope* nonnull %3) #2
  call void @llvm.lifetime.end.p0i8(i64 32, i8* nonnull %4) #2
  ret i32 0
}

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN8LogScopeC2EPKc(%class.LogScope*, i8*) unnamed_addr #5 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %3 = alloca i64, align 8
  %4 = alloca %"class.std::__cxx11::basic_string", align 8
  %5 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %4, i64 0, i32 2
  %6 = bitcast %"class.std::__cxx11::basic_string"* %4 to %union.anon**
  store %union.anon* %5, %union.anon** %6, align 8, !tbaa !6
  %7 = icmp eq i8* %1, null
  %8 = bitcast %union.anon* %5 to i8*
  br i1 %7, label %13, label %9

; <label>:9:                                      ; preds = %2
  %10 = call i64 @strlen(i8* nonnull %1) #2
  %11 = bitcast i64* %3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* nonnull %11) #2
  store i64 %10, i64* %3, align 8, !tbaa !8
  %12 = icmp ugt i64 %10, 15
  br i1 %12, label %17, label %15

; <label>:13:                                     ; preds = %2
  invoke void @_ZSt19__throw_logic_errorPKc(i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.7, i64 0, i64 0)) #11
          to label %14 unwind label %39

; <label>:14:                                     ; preds = %13
  unreachable

; <label>:15:                                     ; preds = %9
  %16 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %4, i64 0, i32 0, i32 0
  br label %23

; <label>:17:                                     ; preds = %9
  %18 = invoke i8* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm(%"class.std::__cxx11::basic_string"* nonnull %4, i64* nonnull dereferenceable(8) %3, i64 0)
          to label %19 unwind label %39

; <label>:19:                                     ; preds = %17
  %20 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %4, i64 0, i32 0, i32 0
  store i8* %18, i8** %20, align 8, !tbaa !10
  %21 = load i64, i64* %3, align 8, !tbaa !8
  %22 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %4, i64 0, i32 2, i32 0
  store i64 %21, i64* %22, align 8, !tbaa !12
  br label %23

; <label>:23:                                     ; preds = %19, %15
  %24 = phi i8** [ %16, %15 ], [ %20, %19 ]
  %25 = phi i8* [ %8, %15 ], [ %18, %19 ]
  switch i64 %10, label %28 [
    i64 1, label %26
    i64 0, label %29
  ]

; <label>:26:                                     ; preds = %23
  %27 = load i8, i8* %1, align 1, !tbaa !12
  store i8 %27, i8* %25, align 1, !tbaa !12
  br label %29

; <label>:28:                                     ; preds = %23
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %25, i8* nonnull %1, i64 %10, i32 1, i1 false) #2
  br label %29

; <label>:29:                                     ; preds = %28, %26, %23
  %30 = load i64, i64* %3, align 8, !tbaa !8
  %31 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %4, i64 0, i32 1
  store i64 %30, i64* %31, align 8, !tbaa !13
  %32 = load i8*, i8** %24, align 8, !tbaa !10
  %33 = getelementptr inbounds i8, i8* %32, i64 %30
  store i8 0, i8* %33, align 1, !tbaa !12
  call void @llvm.lifetime.end.p0i8(i64 8, i8* nonnull %11) #2
  call void @_ZN8LogScopeC2ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.LogScope* %0, %"class.std::__cxx11::basic_string"* nonnull %4) #2
  %34 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %4, i64 0, i32 0, i32 0
  %35 = load i8*, i8** %34, align 8, !tbaa !10
  %36 = icmp eq i8* %35, %8
  br i1 %36, label %38, label %37

; <label>:37:                                     ; preds = %29
  call void @_ZdlPv(i8* %35) #2
  br label %38

; <label>:38:                                     ; preds = %29, %37
  ret void

; <label>:39:                                     ; preds = %17, %13
  %40 = landingpad { i8*, i32 }
          catch i8* null
  %41 = extractvalue { i8*, i32 } %40, 0
  call void @__clang_call_terminate(i8* %41) #12
  unreachable
}

declare void @__cxa_end_catch() local_unnamed_addr

; Function Attrs: nounwind uwtable
declare void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev(%"class.std::__cxx11::basic_string"*) unnamed_addr #5 align 2

declare void @__cxa_call_unexpected(i8*) local_unnamed_addr

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l(%"class.std::basic_ostream"* dereferenceable(272), i8*, i64) local_unnamed_addr #0

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) local_unnamed_addr #8

; Function Attrs: nounwind uwtable
define linkonce_odr void @_ZN8LogScopeC2ENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE(%class.LogScope*, %"class.std::__cxx11::basic_string"*) unnamed_addr #5 comdat align 2 personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %3 = getelementptr inbounds %class.LogScope, %class.LogScope* %0, i64 0, i32 0
  %4 = getelementptr inbounds %class.LogScope, %class.LogScope* %0, i64 0, i32 0, i32 2
  %5 = bitcast %class.LogScope* %0 to %union.anon**
  store %union.anon* %4, %union.anon** %5, align 8, !tbaa !6
  %6 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %1, i64 0, i32 0, i32 0
  %7 = load i8*, i8** %6, align 8, !tbaa !10
  %8 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %1, i64 0, i32 2
  %9 = bitcast %union.anon* %8 to i8*
  %10 = icmp eq i8* %7, %9
  br i1 %10, label %11, label %13

; <label>:11:                                     ; preds = %2
  %12 = bitcast %union.anon* %4 to i8*
  tail call void @llvm.memcpy.p0i8.p0i8.i64(i8* nonnull %12, i8* nonnull %7, i64 16, i32 1, i1 false) #2
  br label %18

; <label>:13:                                     ; preds = %2
  %14 = getelementptr inbounds %class.LogScope, %class.LogScope* %0, i64 0, i32 0, i32 0, i32 0
  store i8* %7, i8** %14, align 8, !tbaa !10
  %15 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %1, i64 0, i32 2, i32 0
  %16 = load i64, i64* %15, align 8, !tbaa !12
  %17 = getelementptr inbounds %class.LogScope, %class.LogScope* %0, i64 0, i32 0, i32 2, i32 0
  store i64 %16, i64* %17, align 8, !tbaa !12
  br label %18

; <label>:18:                                     ; preds = %11, %13
  %19 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %1, i64 0, i32 1
  %20 = load i64, i64* %19, align 8, !tbaa !13
  %21 = getelementptr inbounds %class.LogScope, %class.LogScope* %0, i64 0, i32 0, i32 1
  store i64 %20, i64* %21, align 8, !tbaa !13
  %22 = bitcast %"class.std::__cxx11::basic_string"* %1 to %union.anon**
  store %union.anon* %8, %union.anon** %22, align 8, !tbaa !10
  store i64 0, i64* %19, align 8, !tbaa !13
  store i8 0, i8* %9, align 1, !tbaa !12
  %23 = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l(%"class.std::basic_ostream"* nonnull dereferenceable(272) @_ZSt4cout, i8* nonnull getelementptr inbounds ([17 x i8], [17 x i8]* @.str.8, i64 0, i64 0), i64 16)
          to label %24 unwind label %63

; <label>:24:                                     ; preds = %18
  %25 = getelementptr inbounds %class.LogScope, %class.LogScope* %0, i64 0, i32 0, i32 0, i32 0
  %26 = load i8*, i8** %25, align 8, !tbaa !10
  %27 = load i64, i64* %21, align 8, !tbaa !13
  %28 = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l(%"class.std::basic_ostream"* nonnull dereferenceable(272) @_ZSt4cout, i8* %26, i64 %27)
          to label %29 unwind label %63

; <label>:29:                                     ; preds = %24
  %30 = bitcast %"class.std::basic_ostream"* %28 to i8**
  %31 = load i8*, i8** %30, align 8, !tbaa !23
  %32 = getelementptr i8, i8* %31, i64 -24
  %33 = bitcast i8* %32 to i64*
  %34 = load i64, i64* %33, align 8
  %35 = bitcast %"class.std::basic_ostream"* %28 to i8*
  %36 = getelementptr inbounds i8, i8* %35, i64 %34
  %37 = getelementptr inbounds i8, i8* %36, i64 240
  %38 = bitcast i8* %37 to %"class.std::ctype"**
  %39 = load %"class.std::ctype"*, %"class.std::ctype"** %38, align 8, !tbaa !25
  %40 = icmp eq %"class.std::ctype"* %39, null
  br i1 %40, label %41, label %43

; <label>:41:                                     ; preds = %29
  invoke void @_ZSt16__throw_bad_castv() #11
          to label %42 unwind label %63

; <label>:42:                                     ; preds = %41
  unreachable

; <label>:43:                                     ; preds = %29
  %44 = getelementptr inbounds %"class.std::ctype", %"class.std::ctype"* %39, i64 0, i32 8
  %45 = load i8, i8* %44, align 8, !tbaa !28
  %46 = icmp eq i8 %45, 0
  br i1 %46, label %50, label %47

; <label>:47:                                     ; preds = %43
  %48 = getelementptr inbounds %"class.std::ctype", %"class.std::ctype"* %39, i64 0, i32 9, i64 10
  %49 = load i8, i8* %48, align 1, !tbaa !12
  br label %57

; <label>:50:                                     ; preds = %43
  invoke void @_ZNKSt5ctypeIcE13_M_widen_initEv(%"class.std::ctype"* nonnull %39)
          to label %51 unwind label %63

; <label>:51:                                     ; preds = %50
  %52 = bitcast %"class.std::ctype"* %39 to i8 (%"class.std::ctype"*, i8)***
  %53 = load i8 (%"class.std::ctype"*, i8)**, i8 (%"class.std::ctype"*, i8)*** %52, align 8, !tbaa !23
  %54 = getelementptr inbounds i8 (%"class.std::ctype"*, i8)*, i8 (%"class.std::ctype"*, i8)** %53, i64 6
  %55 = load i8 (%"class.std::ctype"*, i8)*, i8 (%"class.std::ctype"*, i8)** %54, align 8
  %56 = invoke signext i8 %55(%"class.std::ctype"* nonnull %39, i8 signext 10)
          to label %57 unwind label %63

; <label>:57:                                     ; preds = %51, %47
  %58 = phi i8 [ %49, %47 ], [ %56, %51 ]
  %59 = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZNSo3putEc(%"class.std::basic_ostream"* nonnull %28, i8 signext %58)
          to label %60 unwind label %63

; <label>:60:                                     ; preds = %57
  %61 = invoke dereferenceable(272) %"class.std::basic_ostream"* @_ZNSo5flushEv(%"class.std::basic_ostream"* nonnull %59)
          to label %62 unwind label %63

; <label>:62:                                     ; preds = %60
  ret void

; <label>:63:                                     ; preds = %41, %51, %50, %60, %57, %24, %18
  %64 = landingpad { i8*, i32 }
          catch i8* null
  %65 = extractvalue { i8*, i32 } %64, 0
  tail call void @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev(%"class.std::__cxx11::basic_string"* nonnull %3) #2
  tail call void @__clang_call_terminate(i8* %65) #12
  unreachable
}

; Function Attrs: noreturn
declare void @_ZSt19__throw_logic_errorPKc(i8*) local_unnamed_addr #9

declare i8* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm(%"class.std::__cxx11::basic_string"*, i64* dereferenceable(8), i64) local_unnamed_addr #0

; Function Attrs: argmemonly nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture writeonly, i8* nocapture readonly, i64, i32, i1) #4

; Function Attrs: argmemonly nounwind readonly
declare i64 @strlen(i8* nocapture) local_unnamed_addr #10

; Function Attrs: nounwind
declare i64 @strtol(i8* readonly, i8** nocapture, i32) local_unnamed_addr #1

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZNSo3putEc(%"class.std::basic_ostream"*, i8 signext) local_unnamed_addr #0

declare dereferenceable(272) %"class.std::basic_ostream"* @_ZNSo5flushEv(%"class.std::basic_ostream"*) local_unnamed_addr #0

; Function Attrs: noreturn
declare void @_ZSt16__throw_bad_castv() local_unnamed_addr #9

declare void @_ZNKSt5ctypeIcE13_M_widen_initEv(%"class.std::ctype"*) local_unnamed_addr #0

declare dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm(%"class.std::__cxx11::basic_string"*, i8*, i64) local_unnamed_addr #0

; Function Attrs: noreturn
declare void @_ZSt20__throw_length_errorPKc(i8*) local_unnamed_addr #9

declare dereferenceable(32) %"class.std::__cxx11::basic_string"* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm(%"class.std::__cxx11::basic_string"*, i64, i64, i8*, i64) local_unnamed_addr #0

; Function Attrs: uwtable
define linkonce_odr void @_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z(%"class.std::__cxx11::basic_string"* noalias sret, i32 (i8*, i64, i8*, %struct.__va_list_tag*)*, i64, i8*, ...) local_unnamed_addr #3 comdat personality i8* bitcast (i32 (...)* @__gxx_personality_v0 to i8*) {
  %5 = alloca i64, align 8
  %6 = alloca [1 x %struct.__va_list_tag], align 16
  %7 = alloca i8, i64 %2, align 16
  %8 = bitcast [1 x %struct.__va_list_tag]* %6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* nonnull %8) #2
  %9 = getelementptr inbounds [1 x %struct.__va_list_tag], [1 x %struct.__va_list_tag]* %6, i64 0, i64 0
  call void @llvm.va_start(i8* nonnull %8)
  %10 = call i32 %1(i8* nonnull %7, i64 %2, i8* %3, %struct.__va_list_tag* nonnull %9)
  call void @llvm.va_end(i8* nonnull %8)
  %11 = sext i32 %10 to i64
  %12 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %0, i64 0, i32 2
  %13 = bitcast %"class.std::__cxx11::basic_string"* %0 to %union.anon**
  store %union.anon* %12, %union.anon** %13, align 8, !tbaa !6
  %14 = bitcast i64* %5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* nonnull %14) #2
  store i64 %11, i64* %5, align 8, !tbaa !8
  %15 = icmp ugt i32 %10, 15
  br i1 %15, label %19, label %16

; <label>:16:                                     ; preds = %4
  %17 = bitcast %union.anon* %12 to i8*
  %18 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %0, i64 0, i32 0, i32 0
  br label %25

; <label>:19:                                     ; preds = %4
  %20 = invoke i8* @_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm(%"class.std::__cxx11::basic_string"* nonnull %0, i64* nonnull dereferenceable(8) %5, i64 0)
          to label %21 unwind label %36

; <label>:21:                                     ; preds = %19
  %22 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %0, i64 0, i32 0, i32 0
  store i8* %20, i8** %22, align 8, !tbaa !10
  %23 = load i64, i64* %5, align 8, !tbaa !8
  %24 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %0, i64 0, i32 2, i32 0
  store i64 %23, i64* %24, align 8, !tbaa !12
  br label %25

; <label>:25:                                     ; preds = %21, %16
  %26 = phi i8** [ %18, %16 ], [ %22, %21 ]
  %27 = phi i8* [ %17, %16 ], [ %20, %21 ]
  switch i64 %11, label %30 [
    i64 1, label %28
    i64 0, label %31
  ]

; <label>:28:                                     ; preds = %25
  %29 = load i8, i8* %7, align 16, !tbaa !12
  store i8 %29, i8* %27, align 1, !tbaa !12
  br label %31

; <label>:30:                                     ; preds = %25
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %27, i8* nonnull %7, i64 %11, i32 1, i1 false) #2
  br label %31

; <label>:31:                                     ; preds = %30, %28, %25
  %32 = load i64, i64* %5, align 8, !tbaa !8
  %33 = getelementptr inbounds %"class.std::__cxx11::basic_string", %"class.std::__cxx11::basic_string"* %0, i64 0, i32 1
  store i64 %32, i64* %33, align 8, !tbaa !13
  %34 = load i8*, i8** %26, align 8, !tbaa !10
  %35 = getelementptr inbounds i8, i8* %34, i64 %32
  store i8 0, i8* %35, align 1, !tbaa !12
  call void @llvm.lifetime.end.p0i8(i64 8, i8* nonnull %14) #2
  call void @llvm.lifetime.end.p0i8(i64 24, i8* nonnull %8) #2
  ret void

; <label>:36:                                     ; preds = %19
  %37 = landingpad { i8*, i32 }
          cleanup
  call void @llvm.lifetime.end.p0i8(i64 24, i8* nonnull %8) #2
  resume { i8*, i32 } %37
}

; Function Attrs: nounwind
declare i32 @vsnprintf(i8* nocapture, i64, i8* nocapture readonly, %struct.__va_list_tag*) #1

; Function Attrs: nounwind
declare void @llvm.va_start(i8*) #2

; Function Attrs: nounwind
declare void @llvm.va_end(i8*) #2

; Function Attrs: uwtable
define internal void @_GLOBAL__sub_I_sum_exception_value.cpp() #3 section ".text.startup" {
  tail call void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"* nonnull @_ZStL8__ioinit)
  %1 = tail call i32 @__cxa_atexit(void (i8*)* bitcast (void (%"class.std::ios_base::Init"*)* @_ZNSt8ios_base4InitD1Ev to void (i8*)*), i8* getelementptr inbounds (%"class.std::ios_base::Init", %"class.std::ios_base::Init"* @_ZStL8__ioinit, i64 0, i32 0), i8* nonnull @__dso_handle) #2
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }
attributes #3 = { uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind }
attributes #5 = { nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { noinline noreturn nounwind }
attributes #7 = { norecurse uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #10 = { argmemonly nounwind readonly "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #11 = { noreturn }
attributes #12 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 6.0.1-svn334776-1~exp1~20190124082841.120 (branches/release_60)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !3, i64 0}
!7 = !{!"_ZTSNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_Alloc_hiderE", !3, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"long", !4, i64 0}
!10 = !{!11, !3, i64 0}
!11 = !{!"_ZTSNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE", !7, i64 0, !9, i64 8, !4, i64 16}
!12 = !{!4, !4, i64 0}
!13 = !{!11, !9, i64 8}
!14 = !{!15}
!15 = distinct !{!15, !16, !"_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_: argument 0"}
!16 = distinct !{!16, !"_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_"}
!17 = !{!18}
!18 = distinct !{!18, !19, !"_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_: argument 0"}
!19 = distinct !{!19, !"_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_"}
!20 = !{!21}
!21 = distinct !{!21, !22, !"_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_: argument 0"}
!22 = distinct !{!22, !"_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_"}
!23 = !{!24, !24, i64 0}
!24 = !{!"vtable pointer", !5, i64 0}
!25 = !{!26, !3, i64 240}
!26 = !{!"_ZTSSt9basic_iosIcSt11char_traitsIcEE", !3, i64 216, !4, i64 224, !27, i64 225, !3, i64 232, !3, i64 240, !3, i64 248, !3, i64 256}
!27 = !{!"bool", !4, i64 0}
!28 = !{!29, !4, i64 56}
!29 = !{!"_ZTSSt5ctypeIcE", !3, i64 16, !27, i64 24, !3, i64 32, !3, i64 40, !3, i64 48, !4, i64 56, !4, i64 57, !4, i64 313, !4, i64 569}
