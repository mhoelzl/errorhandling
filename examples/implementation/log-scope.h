#include <utility>

//
// Created by tc on 10.04.19.
//

#ifndef ERRORHANDLING_LOG_SCOPE_H
#define ERRORHANDLING_LOG_SCOPE_H

#include <iostream>

// All members in LogScope are noexcept to simplify the generated assembly; this will abort the program if something
// unexpected happens.

class LogScope
{
public:
	explicit LogScope(std::string Name) noexcept : ScopeName{std::move(Name)}
	{
		std::cout << "Entering scope: " << ScopeName << std::endl;
	}

	explicit LogScope(const char* Name) noexcept : LogScope{std::string{Name}}
	{
	}

	template <typename T>
	LogScope(const char* FunctionName, T arg) noexcept
		: LogScope(std::string{FunctionName} + "(" + std::to_string(arg) + ")")
	{
	}

	~LogScope()
	{
		std::cout << "Exiting scope: " << ScopeName << std::endl;
	}

private:
	std::string ScopeName;
};

#endif // ERRORHANDLING_LOG_SCOPE_H
