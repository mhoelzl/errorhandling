#include <cstdlib>
#include <iostream>

#include "expected.h"
#include "log-scope.h"

using namespace ::std::literals::string_literals;

expected<int> sum(int n, int count = 0) noexcept
{
	LogScope logScope{"sum", n};

	if (n <= 0)
	{
		if (count % 2 == 0)
		{
			return unexpected{"Argument out of range"};
		}
		else
		{
			return 0;
		}
	}

	expected<int> result = sum(n - 1, count + 1);
	if (result.is_valid())
	{
		return result.get_value() + n;
	}
	else
	{
		return result;
	}
}

void print_result(int input, int result) noexcept
{
	std::cout << "The sum from 1 to " << input << " is " << result << std::endl;
}

void print_error_message() noexcept
{
	std::cout << "Encountered an error somewhere during execution." << std::endl;
}

int main(int argc, char** argv)
{
	LogScope logScope{"main"};
	if (argc > 1)
	{
		int input{std::atoi(argv[1])};
		expected<int> result{sum(input)};
		if (result.is_valid())
		{
			print_result(input, result.get_value());
		}
		else
		{
			print_error_message();
		}
	}
	return 0;
}
